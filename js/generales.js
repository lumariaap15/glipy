$(function () {
    /**
     * SCRIPTS PARA CUSTOMIZAR EL INPUT[TYPE=FILE]
     */
    $('.myfile-group').each(
        function () {
            var resultado = $(this).children('p').first()
            $(this).find('input').first().change(
                function () {
                    filename = $(this).prop('files')[0].name
                    resultado.html(filename)
                }
            )
        }
    )

    $('.dropdown').click(function () {
        menu = $(this).find('.dropdown-menu.show').first()
        menu.css('transform', 'none')
    });


    $('.myinput-container-buscar').each(
        function () {
            var contenedor = $(this)
            var inputHijo = contenedor.find('input.myinput').first();
            inputHijo.focus(function () {
                contenedor.addClass('quitar-after')
            })
            inputHijo.focusout(function () {
                contenedor.removeClass('quitar-after')
            })

        }
    )

    $('.myinput-material').each(
        function () {
            var contenedor = $(this)
            var inputHijo = contenedor.find('input.myinput').first();
            inputHijo.focus(function () {
                contenedor.addClass('filled')
            })
            inputHijo.focusout(function () {
                if ($(this).val() !== '') {
                    contenedor.addClass('filled')
                } else {
                    contenedor.removeClass('filled')
                }
            })
            inputHijo.change(function () {

            })
        }
    )

    $('.input-buttons-group').each(
        function () {
            var input = $(this).find('input').first()
            $(this).find('button').each(
                function () {
                    $(this).click(function () {
                        if ($(this).html() === '+') {
                            valor = input.val().split('U')
                            valor = Number(valor[0]) + 1
                            input.val(valor + " UND")
                        } else {
                            valor = input.val().split('U')
                            valor = Number(valor[0]) - 1
                            input.val(valor + " UND")
                        }
                    })
                }
            )
        }
    )

    $('.migaleria').each(
        function(){
            var resultadoContainer = $(this).find('.img-selected').first()
            $(this).find('.opc-imagen').each(
                function(){
                    $(this).click(function(){
                        resultadoContainer.css('background-image','url('+$(this).data('img')+')')
                        removeSelectedGaleria()
                        $(this).addClass('selected')
                    })
                }
            )
            $(this).find('.opc-imagen').first().trigger('click')
        }
    )
})

function animateCSS(element, animationName, callback) {
    const node = document.querySelector(element);

    node.classList.add('animated', animationName)

    function handleAnimationEnd() {
        node.classList.remove('animated', animationName)
        node.removeEventListener('animationend', handleAnimationEnd)

        if (typeof callback === 'function') callback()
    }

    node.addEventListener('animationend', handleAnimationEnd)

}

function esconderSecciones(contenedor, hijo) {
    $(contenedor).first().find(hijo).each(
        function () {
            var target = $(this).data('mostrar')
            $('#' + target).css('display', 'none')
            $(this).removeClass('selected')
        }
    )
}

function Navegacion(contenedor, hijo, animacion = 'fadeInUp', seleccionarPrimero = true) {
    $(contenedor).first().find(hijo).each(
        function () {
            var target = $(this).data('mostrar')
            $(this).click(function () {
                esconderSecciones(contenedor, hijo)
                animateCSS('#' + target, animacion)
                $('#' + target).css('display', 'block')
                $(this).addClass('selected')
            })
        }
    )
    esconderSecciones(contenedor)
    if (seleccionarPrimero) {
        $(contenedor).first().find(hijo).first().trigger('click')
    }
}

function removeSelectedGaleria(){
    $('.migaleria').each(
        function(){
            $(this).find('.opc-imagen').each(
                function(){
                    $(this).removeClass('selected')
                }
            )
        }
    )
}
/*
Estilos de los select (custom)
*/
/*
$('select').each(function(){
    var $this = $(this), numberOfOptions = $(this).children('option').length;
  
    $this.addClass('select-hidden'); 
    $this.wrap('<div class="select"></div>');
    $this.after('<div class="select-styled"></div>');

    var $styledSelect = $this.next('div.select-styled');
    $styledSelect.text($this.children('option').eq(0).text());
  
    var $list = $('<ul />', {
        'class': 'select-options'
    }).insertAfter($styledSelect);
  
    for (var i = 0; i < numberOfOptions; i++) {
        $('<li />', {
            text: $this.children('option').eq(i).text(),
            rel: $this.children('option').eq(i).val()
        }).appendTo($list);
    }
  
    var $listItems = $list.children('li');
  
    $styledSelect.click(function(e) {
        e.stopPropagation();
        $('div.select-styled.active').not(this).each(function(){
            $(this).removeClass('active').next('ul.select-options').hide();
        });
        $(this).toggleClass('active').next('ul.select-options').toggle();
    });
  
    $listItems.click(function(e) {
        e.stopPropagation();
        $styledSelect.text($(this).text()).removeClass('active');
        $this.val($(this).attr('rel'));
        $list.hide();
        //console.log($this.val());
    });
  
    $(document).click(function() {
        $styledSelect.removeClass('active');
        $list.hide();
    });

});*/
